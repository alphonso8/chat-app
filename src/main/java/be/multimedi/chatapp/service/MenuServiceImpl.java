package be.multimedi.chatapp.service;

import be.multimedi.chatapp.model.FriendRequest;
import be.multimedi.chatapp.model.User;
import be.multimedi.chatapp.repository.UserRepository;
import be.multimedi.chatapp.repository.UserRepositoryImpl;
import be.multimedi.chatapp.util.KeyboardUtility;

public class MenuServiceImpl implements MenuService{

    private  final UserRepository userRepo = new UserRepositoryImpl();
    private  User loggedInUser;


    @Override
    public void register() {
            User newUser = askRegisterUser();
            userRepo.save(newUser);
    }

    @Override
    public void login() {
        String userName;
        String password;


        userName = KeyboardUtility.ask("Please enter your username");
        password = KeyboardUtility.ask("Please enter your password");

//        compare usernames
        User account = userRepo.findByUserName(userName);
        if (account == null){
            System.out.println("Sorry, there is no account with this name");
        }
        else{
            if (password.equals(account.getPassword())){
                loggedInUser = account;
                System.out.println("You have logged in");
                showMenu();
            }else{
                System.out.println("this combination does not exist");
            }
        }
    }

    @Override
    public void showMenu() {
        System.out.println("~Options~");
               String [] menuOptions = {
                "Send friendrequest",
                "Show Friendslist",
                "Show friendrequest"

        };

        switch (KeyboardUtility.askForChoice(menuOptions)){
            case 1:
                sendFriendRequest();
                break;
            case 2:
                System.out.println("insert method");
                break;
            case 3:
                System.out.println("Insert method");
                break;
            default:
                System.out.println("please choose a valid option");
        }

    }

    @Override
    public void sendFriendRequest() {
        String userName = KeyboardUtility.ask("Please enter a username you wish to add");
        User searchedUser = userRepo.findByUserName(userName);
        if (searchedUser == null){
            System.out.println("Sorry, there is no account with this name");
        } else {
            FriendRequest friendRequest = new FriendRequest();
            friendRequest.setSender(loggedInUser);
            friendRequest.setRecipient(searchedUser);

            loggedInUser.getSentRequests().add(friendRequest);
            searchedUser.getReceivedRequests().add(friendRequest);
        }

    }


    private boolean checkPassword(String pass , User account){
        boolean isPassCorrect = userRepo.findAccountByPassword(pass, account);
        return isPassCorrect;
    }

    private User askRegisterUser() {
        User user = new User();
        user.setEmailaddress(KeyboardUtility.askForEmail("Give your email:"));
        user.setUsername(KeyboardUtility.ask("Choose a usernamer:"));
        user.setPassword(KeyboardUtility.ask("Choose a password:"));
        return user;
    }


}
