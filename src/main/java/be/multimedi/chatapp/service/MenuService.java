package be.multimedi.chatapp.service;

public interface MenuService {
    void register();
    void login();
    void showMenu();
    void sendFriendRequest();



}
