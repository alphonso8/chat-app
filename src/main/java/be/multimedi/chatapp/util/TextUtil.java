package be.multimedi.chatapp.util;

public class TextUtil {
    public static void printTitle(String titel) {
        String lijn = generateLine('=', titel.length());
        String titleNice = String.format("%s%n%s%n%s", lijn, titel, lijn);
        System.out.println(titleNice);
    }

    public static void printSubheading(String titel) {
        String lijn = generateLine('-', titel.length());
        String titleNice = String.format("%s%n%s%n%s", lijn, titel, lijn);
        System.out.println(titleNice);
    }

    public static String generateLine(char c, int length) {
        StringBuilder lijnAsSB = new StringBuilder();
        for (int i = 0; i < length; i++) {
            lijnAsSB.append(c);
        }
        return lijnAsSB.toString();
    }

    public static String snakeCaseToTitleCase(String sentence){
        String nonSnake = sentence.replace('_', ' ');
        String[] words = nonSnake.split(" ");
        return titleCase(words);
    }

    public static String titleCase(String word) {
        StringBuilder result = new StringBuilder();
        char[] letters = word.toCharArray();
        result.append(Character.toUpperCase(letters[0]));
        for (int i = 1; i < letters.length; i++) {
            result.append(Character.toLowerCase(letters[i]));
        }
        return result.toString();
    }

    public static String titleCase(String[] words){
        StringBuilder result = new StringBuilder();
        for (String word : words) {
            result.append(titleCase(word));
            result.append(' ');
        }
        return result.toString();
    }

}
