package be.multimedi.chatapp.repository;


import be.multimedi.chatapp.model.User;

public interface UserRepository {
    void save(User user);
    void credentialControl();
    User findByUserName(String userName);
    Boolean findAccountByPassword(String pass, User userAccount);
}
