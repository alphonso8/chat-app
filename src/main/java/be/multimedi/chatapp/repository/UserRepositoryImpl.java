package be.multimedi.chatapp.repository;

import be.multimedi.chatapp.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;

public class UserRepositoryImpl implements UserRepository {

    static EntityManagerFactory emf = Persistence.createEntityManagerFactory("myPersistenceUnit");
    static EntityManager em = emf.createEntityManager();


    @Override
    public void save(User user) {
        em.getTransaction().begin();
        em.persist(user);
        em.getTransaction().commit();

    }

    public User findByUserName(String userName) {
        TypedQuery<User> accountQuery = em.createQuery("SELECT u FROM User u WHERE u.username =:username", User.class);
        accountQuery.setParameter("username", userName);
        List<User> list = accountQuery.getResultList();
//        User userAccount = null;
//        if (list.size() > 0) {
//            userAccount = list.get(0);
//        }
        if(list.size() > 0 && list.get(0) != null){
            return list.get(0);
        } else {
            throw new RuntimeException("No user with username " + userName);
        }
    }

    public Boolean findAccountByPassword(String pass, User userAccount) {
        User retrieveAccount = em.find(User.class, userAccount.getId());
        if (retrieveAccount.getPassword().equals(pass)) {
            return true;
        } else {
            System.out.println("Combination does not exist!");
            return false;

        }


    }

    @Override
    public void credentialControl() {


    }
}
