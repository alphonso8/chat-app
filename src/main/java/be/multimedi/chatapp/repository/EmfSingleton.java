package be.multimedi.chatapp.repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EmfSingleton {
    static EntityManagerFactory emf = Persistence.createEntityManagerFactory("myPersistenceUnit");
    static EntityManager em = emf.createEntityManager();
}
