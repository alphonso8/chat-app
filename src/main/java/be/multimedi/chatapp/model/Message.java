package be.multimedi.chatapp.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "content", length = 255)
    private String body;
    private LocalDateTime timestamp;
    @ManyToOne
    @JoinColumn(name = "sender_id", foreignKey = @ForeignKey(name = "Fk_message_sender_id"))
    private User sender;
    @ManyToOne
    @JoinColumn(name = "recipient_id", foreignKey = @ForeignKey(name = "Fk_message_recipient_id"))
    private User recipient;


    public Message() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getRecipient() {
        return recipient;
    }

    public void setRecipient(User recipient) {
        this.recipient = recipient;
    }
}
