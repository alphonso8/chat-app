//package be.multimedi.chatapp.model;
//
//import javax.persistence.*;
//
//@Entity
//public class Friendship {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
//    @ManyToOne
//    @JoinColumn(name = "user1_id", foreignKey = @ForeignKey(name = "Fk_user1_id"))
//    private User user1;
//    @ManyToOne
//    @JoinColumn(name = "user2_id", foreignKey = @ForeignKey(name = "Fk_user2_id"))
//    private User user2;
//
//    public Friendship() {
//    }
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public User getUser1() {
//        return user1;
//    }
//
//    public void setUser1(User user1) {
//        this.user1 = user1;
//    }
//
//    public User getUser2() {
//        return user2;
//    }
//
//    public void setUser2(User user2) {
//        this.user2 = user2;
//    }
//}
