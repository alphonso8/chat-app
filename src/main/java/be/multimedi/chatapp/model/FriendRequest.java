package be.multimedi.chatapp.model;


import javax.persistence.*;

@Entity
public class FriendRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "sender_id", foreignKey = @ForeignKey(name = "Fk_sender_id"))
    private User sender;
    @ManyToOne
    @JoinColumn(name = "recipient_id", foreignKey = @ForeignKey(name = "Fk_recipient_id"))
    private User recipient;

    public FriendRequest() {
    }

    public FriendRequest(User sender, User recipient) {
        this.sender = sender;
        this.recipient = recipient;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getRecipient() {
        return recipient;
    }

    public void setRecipient(User recipient) {
        this.recipient = recipient;
    }
}
